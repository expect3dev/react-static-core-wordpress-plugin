<?php

//* Company Settings Settings Page */
class companysettings_Settings_Page {
	public function __construct() {
		add_action( 'admin_menu', array( $this, 'wph_create_settings' ) );
		add_action( 'admin_init', array( $this, 'wph_setup_sections' ) );
		add_action( 'admin_init', array( $this, 'wph_setup_fields' ) );
		add_action( 'admin_footer', array( $this, 'media_fields' ) );
		add_action( 'admin_enqueue_scripts', 'wp_enqueue_media' );
	}
	public function wph_create_settings() {
		$page_title = 'Company Information';
		$menu_title = 'Company Info';
		$capability = 'manage_options';
		$slug = 'companysettings';
		$callback = array($this, 'wph_settings_content');
		$icon = 'dashicons-clipboard';
		$position = 2;
		add_menu_page($page_title, $menu_title, $capability, $slug, $callback, $icon, $position);
	}
	public function wph_settings_content() { ?>
		<div class="wrap">
			<h1>Company Information</h1>
			<?php settings_errors(); ?>
			<form method="POST" action="options.php">
				<?php
					settings_fields( 'companysettings' );
					do_settings_sections( 'companysettings' );
					submit_button();
				?>
			</form>
		</div> <?php
	}
	public function wph_setup_sections() {
		add_settings_section( 'companysettings_section', '', array(), 'companysettings' );
	}
	public function wph_setup_fields() {
		$fields = array(
			array(
				'label' => 'Company Logo',
				'id' => 'companyLogo',
				'type' => 'media',
				'section' => 'companysettings_section',
			),
			array(
				'label' => 'Street Address',
				'id' => 'companyAddressStreet',
				'type' => 'text',
				'section' => 'companysettings_section',
			),
			array(
				'label' => 'City',
				'id' => 'companyAddressCity',
				'type' => 'text',
				'section' => 'companysettings_section',
			),
			array(
				'label' => 'State',
				'id' => 'companyAddressState',
				'type' => 'text',
				'section' => 'companysettings_section',
			),
			array(
				'label' => 'Zip Code',
				'id' => 'companyAddressZip',
				'type' => 'text',
				'section' => 'companysettings_section',
			),
			array(
				'label' => 'Location: Latitude',
				'id' => 'companyLocationLat',
				'type' => 'text',
				'section' => 'companysettings_section',
			),
			array(
				'label' => 'Location: Longitude',
				'id' => 'companyLocationLong',
				'type' => 'text',
				'section' => 'companysettings_section',
			),
			array(
				'label' => 'Google Analytics Tracking ID',
				'id' => 'googleAnalyticsId',
				'type' => 'text',
				'section' => 'companysettings_section',
			),
			array(
				'label' => 'Contact Form Email',
				'id' => 'contactFormEmail',
				'type' => 'text',
				'section' => 'companysettings_section',
			),
			array(
				'label' => 'SEO Description',
				'id' => 'metaDescription',
				'type' => 'text',
				'section' => 'companysettings_section',
			),
			array(
				'label' => 'SEO Keywords',
				'id' => 'metaKeywords',
				'type' => 'text',
				'section' => 'companysettings_section',
			),
			array(
				'label' => 'Facebook',
				'id' => 'companySocialFacebook',
				'type' => 'text',
				'section' => 'companysettings_section',
			),
			array(
				'label' => 'Twitter',
				'id' => 'companySocialTwitter',
				'type' => 'text',
				'section' => 'companysettings_section',
			),
			array(
				'label' => 'Instagram',
				'id' => 'companySocialInstagram',
				'type' => 'text',
				'section' => 'companysettings_section',
			),
			array(
				'label' => 'LinkedIn',
				'id' => 'companySocialLinkedIn',
				'type' => 'text',
				'section' => 'companysettings_section',
			),
			array(
				'label' => 'Google+',
				'id' => 'companySocialGoogle',
				'type' => 'text',
				'section' => 'companysettings_section',
			),
			array(
				'label' => 'Pinterest',
				'id' => 'companySocialPinterest',
				'type' => 'text',
				'section' => 'companysettings_section',
			),
			array(
				'label' => 'YouTube',
				'id' => 'companySocialYouTube',
				'type' => 'text',
				'section' => 'companysettings_section',
			),
			array(
				'label' => 'Vimeo',
				'id' => 'companySocialVimeo',
				'type' => 'text',
				'section' => 'companysettings_section',
			),
		);
		foreach( $fields as $field ){
			add_settings_field( $field['id'], $field['label'], array( $this, 'wph_field_callback' ), 'companysettings', $field['section'], $field );
			register_setting( 'companysettings', $field['id'] );
		}
	}
	public function wph_field_callback( $field ) {
		$value = get_option( $field['id'] );
		switch ( $field['type'] ) {
				case 'media':
					printf(
						'<input style="width: 40%%" id="%s" name="%s" type="text" value="%s"> <input style="width: 19%%" class="button companysettings-media" id="%s_button" name="%s_button" type="button" value="Upload" />',
						$field['id'],
						$field['id'],
						$value,
						$field['id'],
						$field['id']
					);
					break;
			default:
				printf( '<input name="%1$s" id="%1$s" type="%2$s" placeholder="%3$s" value="%4$s" />',
					$field['id'],
					$field['type'],
					$field['placeholder'],
					$value
				);
		}
		if( $desc = $field['desc'] ) {
			printf( '<p class="description">%s </p>', $desc );
		}
	}	public function media_fields() {
		?><script>
			jQuery(document).ready(function($){
				if ( typeof wp.media !== 'undefined' ) {
					var _custom_media = true,
					_orig_send_attachment = wp.media.editor.send.attachment;
					$('.companysettings-media').click(function(e) {
						var send_attachment_bkp = wp.media.editor.send.attachment;
						var button = $(this);
						var id = button.attr('id').replace('_button', '');
						_custom_media = true;
							wp.media.editor.send.attachment = function(props, attachment){
							if ( _custom_media ) {
								$('input#'+id).val(attachment.url);
							} else {
								return _orig_send_attachment.apply( this, [props, attachment] );
							};
						}
						wp.media.editor.open(button);
						return false;
					});
					$('.add_media').on('click', function(){
						_custom_media = false;
					});
				}
			});
		</script><?php
	}

}
new companysettings_Settings_Page();

?>